package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class AppDelegate {
	@Autowired
	RestTemplate restTemplate;
	
	@Bean
	@LoadBalanced
	RestTemplate template() {
		return new RestTemplate();
	}

	@HystrixCommand(fallbackMethod ="showFallBackMessage")
	public String loadUsers() {
		return restTemplate.exchange("http://client-service/mainapp/lu",
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<String>() {
				}).getBody();
	}
	
	@HystrixCommand(fallbackMethod ="showFallBackMessage")
	public String loadJPAUsers() {
		return restTemplate.exchange("http://client-service-jpa/mainapp1/loadusers",
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<String>() {
				}).getBody();
	}
	
	private String showFallBackMessage() {
		return "Something went wrong. Try again later";
	}
}
