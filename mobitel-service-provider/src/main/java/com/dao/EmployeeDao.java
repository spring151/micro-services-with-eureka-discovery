package com.dao;

import org.springframework.stereotype.Service;

import com.model.User;
import com.model.Users;

@Service
public class EmployeeDao {

	private static Users list = new Users();
	
	static {
		list.getUserList().add(new User("admin","admin123","admin@mail.com","colombo"));
		list.getUserList().add(new User("manager","manager123","manager@mail.com","matara"));
		list.getUserList().add(new User("qa","qa123","qa@mail.com","kahawa"));
	}
	
	public Users getAllUsers() {
		return list;
	}
	
	public void addUser(User user) {
		list.getUserList().add(user);
	}
}
