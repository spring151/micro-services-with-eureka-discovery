package com.service;

import java.util.List;

import com.model.User;

public interface UserService {

	public void addUser(User user);
	
	public List<User> loadUser();
	
	public boolean findUser(String name);
	
	public boolean deleteUser(String name);
	
	public void updateUser(String name,User user);
}
