package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	ArrayList<User> al = new ArrayList<>();

	@Override
	public void addUser(User user) {
		userDao.save(user);
//		System.out.println(al);

	}

	@Override
	public List<User> loadUser() {
		List<User> list = (List<User>) userDao.findAll();
		return list;
	}

	@Override
	public boolean findUser(String name) {
		Optional<User> data = userDao.findById(name);
		if (data.isPresent()) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteUser(String name) {
		Optional<User> data = userDao.findById(name);
		if (data.isPresent()) {
			userDao.deleteById(name);
			return true;
		}
		return false;

	}

	@Override
	public void updateUser(String name, User user) {
		Optional<User> data = userDao.findById(name);
		if (data.isPresent()) {
			data.get().setEmail(user.getEmail());
			data.get().setPassword(user.getPassword());
			data.get().setCity(user.getCity());
		}

	}

}
