package com.example.mobitelserviceprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com")
@EnableEurekaClient
@EnableJpaRepositories("com.dao")
@EntityScan("com.model")
public class MobitelServiceProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobitelServiceProviderApplication.class, args);
	}

}
