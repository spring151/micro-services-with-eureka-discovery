package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.User;
import com.service.UserService;

@RestController
@RequestMapping("/mainapp1")
public class MainApp {

	@Autowired
	UserService userService;

	@Value("${spring.application.name}")
	public String name;
	
	ArrayList<User> al = new ArrayList<>();

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String sayWelcome() {

		return "Welcome to Spring";
	}

	@PostMapping("/register")
	@ResponseBody
	public String register(@ModelAttribute User user) {
		userService.addUser(user);
		return "user registered";
	}

	@PostMapping("/register2")
	public String register2(@RequestBody User user) {
		userService.addUser(user);
		return "user registered";
	}
	
	@GetMapping("/loadusers")
	@ResponseBody
	public List<User> loadUsers(@ModelAttribute User user) {
		return userService.loadUser();
	}

	@GetMapping("/finduser/{name}")
	@ResponseBody
	public String findUser(@PathVariable String name) {
		if(userService.findUser(name)) {
			return name +" found";
		}
		return "user not found";
	}

	@GetMapping("/deleteuser/{name}")
	@ResponseBody
	public String deleteUser(@PathVariable String name) {
		if(userService.deleteUser(name)) {
			return name +" found and deleted";
		}
		return "user not found";
	}
	
	@PutMapping("/updateuser/{name}")
	@ResponseBody
	public String updateUser(@PathVariable String name, @RequestBody User user) {
		userService.updateUser(name,user);
			return name +" found and updated";
		
	}
}
